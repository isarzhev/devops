package com.backend.spring.jpa.h2;

import com.backend.spring.jpa.h2.model.Tutorial;
import com.backend.spring.jpa.h2.repository.TutorialRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringBootJpaH2ApplicationTests {
	@Autowired
	TutorialRepository tutorialRepository;

	List<Tutorial> data;

	@BeforeEach
	void setup() {
		tutorialRepository.deleteAll();
		data = List.of(
			new Tutorial("title 1 test", "description1", false),
			new Tutorial("title 2", "description2", true)
		);
		tutorialRepository.saveAll(data);
	}

	@Test
	void contextLoads() {
	}

	@Test
	void getAllPublished() {
		var published  = tutorialRepository.findByPublished(true);
		var unpublished = tutorialRepository.findByPublished(false);

		Assertions.assertArrayEquals(
				data.stream().filter(Tutorial::isPublished).map(Tutorial::getId).toArray(),
				published.stream().map(Tutorial::getId).toArray());
		Assertions.assertArrayEquals(
				data.stream().filter(t -> !t.isPublished()).map(Tutorial::getId).toArray(),
				unpublished.stream().map(Tutorial::getId).toArray());
	}

	@Test
	void findByName() {
		Assertions.assertArrayEquals(
				data.stream().filter(t -> t.getTitle().contains("test")).map(Tutorial::getId).toArray(),
				tutorialRepository.findByTitleContainingIgnoreCase("test").stream().map(Tutorial::getId).toArray());
	}
}
